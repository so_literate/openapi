package openapi

import (
	"gitlab.com/so_literate/object"
)

// PathObjectItem is a relative path to an individual endpoint.
// The field name MUST begin with a forward slash (/).
// The path is appended (no relative URL resolution) to the expanded URL from the
// Server Object's url field in order to construct the full URL.
// Path templating is allowed. When matching URLs, concrete (non-templated) paths
// would be matched before their templated counterparts.
// Templated paths with the same hierarchy but different templated names
// MUST NOT exist as they are identical.
// In case of ambiguous matching, it's up to the tooling to decide which one to use.
type PathObjectItem struct {
	Path   string
	Object *PathItem
}

// PathObject holds the relative paths to the individual endpoints and their operations.
// The path is appended to the URL from the Server Object in order to construct the full URL.
// The Paths MAY be empty, due to ACL constraints.
type PathObject []PathObjectItem

// MarshalJSON implements JSON Marshaler interface.
func (o PathObject) MarshalJSON() ([]byte, error) {
	return object.MarshalJSON(len(o), func(i int) (string, interface{}) {
		value := o[i]
		return value.Path, value.Object
	})
}

// UnmarshalJSON implements JSON Unmarshaler interface.
func (o *PathObject) UnmarshalJSON(data []byte) error {
	var res PathObject

	defer func() { *o = res }()

	return object.UnmarshalJSON(
		data,
		func() interface{} { return new(PathItem) },
		func(key string, value interface{}) {
			res = append(res, PathObjectItem{
				Path:   key,
				Object: value.(*PathItem),
			})
		},
	)
}

// SchemaObjectItem object's item.
type SchemaObjectItem struct {
	Name   string
	Schema *Schema
}

// SchemaObject is an object to hold reusable Schema Objects.
type SchemaObject []SchemaObjectItem

// MarshalJSON implements JSON Marshaler interface.
func (o SchemaObject) MarshalJSON() ([]byte, error) {
	return object.MarshalJSON(len(o), func(i int) (string, interface{}) {
		value := o[i]
		return value.Name, value.Schema
	})
}

// UnmarshalJSON implements JSON Unmarshaler interface.
func (o *SchemaObject) UnmarshalJSON(data []byte) error {
	var res SchemaObject

	defer func() { *o = res }()

	return object.UnmarshalJSON(
		data,
		func() interface{} { return new(Schema) },
		func(key string, value interface{}) {
			res = append(res, SchemaObjectItem{
				Name:   key,
				Schema: value.(*Schema),
			})
		},
	)
}

// SecurityRequirementObjectItem is an item of the object.
// Each name MUST correspond to a security scheme which is declared in the
// Security Schemes under the Components Object.
// If the security scheme is of type "oauth2" or "openIdConnect", then the value is a
// list of scope names required for the execution, and the list MAY be empty if authorization
// does not require a specified scope. For other security scheme types, the array MUST be empty.
type SecurityRequirementObjectItem struct {
	Name string
	List []string
}

// SecurityRequirementObject is a lists the required security schemes to execute this operation.
// The name used for each property MUST correspond to a security scheme declared in the
// Security Schemes under the Components Object.
//
// Security Requirement Objects that contain multiple schemes require that all schemes
// MUST be satisfied for a request to be authorized.
// This enables support for scenarios where multiple query parameters or HTTP headers are
// required to convey security information.
//
// When a list of Security Requirement Objects is defined on the OpenAPI Object or
// Operation Object, only one of the Security Requirement Objects in the list needs
// to be satisfied to authorize the request.
type SecurityRequirementObject []SecurityRequirementObjectItem

// MarshalJSON implements JSON Marshaler interface.
func (o SecurityRequirementObject) MarshalJSON() ([]byte, error) {
	return object.MarshalJSON(len(o), func(i int) (string, interface{}) {
		value := o[i]
		return value.Name, value.List
	})
}

// UnmarshalJSON implements JSON Unmarshaler interface.
func (o *SecurityRequirementObject) UnmarshalJSON(data []byte) error {
	var res SecurityRequirementObject

	defer func() { *o = res }()

	return object.UnmarshalJSON(
		data,
		func() interface{} {
			var list []string
			return &list
		},
		func(key string, value interface{}) {
			list := value.(*[]string)
			res = append(res, SecurityRequirementObjectItem{
				Name: key,
				List: *list,
			})
		},
	)
}

// SecuritySchemeObjectItem is an item of the SecuritySchemeObject.
type SecuritySchemeObjectItem struct {
	Name   string
	Scheme *SecurityScheme
}

// SecuritySchemeObject is an object to hold reusable Security Scheme Objects.
type SecuritySchemeObject []SecuritySchemeObjectItem

// MarshalJSON implements JSON Marshaler interface.
func (o SecuritySchemeObject) MarshalJSON() ([]byte, error) {
	return object.MarshalJSON(len(o), func(i int) (string, interface{}) {
		value := o[i]
		return value.Name, value.Scheme
	})
}

// UnmarshalJSON implements JSON Unmarshaler interface.
func (o *SecuritySchemeObject) UnmarshalJSON(data []byte) error {
	var res SecuritySchemeObject

	defer func() { *o = res }()

	return object.UnmarshalJSON(
		data,
		func() interface{} { return new(SecurityScheme) },
		func(key string, value interface{}) {
			res = append(res, SecuritySchemeObjectItem{
				Name:   key,
				Scheme: value.(*SecurityScheme),
			})
		},
	)
}
