package openapi_test

import (
	"bytes"
	"encoding/json"
	"os"
	"path/filepath"
	"testing"

	"gitlab.com/so_literate/openapi"
)

var rpcError = &openapi.Schema{
	Reference:   openapi.Reference{Name: "rpcError"},
	Title:       "rpcError",
	Description: `REQUIRED on error. This member MUST NOT exist if there was no error triggered during invocation.`,
	Type:        openapi.SchemaTypeObject,
	Properties: openapi.SchemaObject{
		{
			Name: "code",
			Schema: &openapi.Schema{
				Description: `A Number that indicates the error type that occurred.`,
				Type:        openapi.SchemaTypeInteger,
				Format:      openapi.SchemaFormatInt64,
			},
		},
		{
			Name: "message",
			Schema: &openapi.Schema{
				Description: `A String providing a short description of the error.`,
				Type:        openapi.SchemaTypeString,
			},
		},
		{
			Name: "data",
			Schema: &openapi.Schema{
				Description: `A Primitive or Structured value that contains additional information about the error.`,
				Type:        openapi.SchemaTypeObject,
			},
		},
	},
}

var params = &openapi.Schema{
	Reference:   openapi.Reference{Name: "Input"},
	Title:       "Input",
	Description: "Input params of the method_name.",
	Type:        openapi.SchemaTypeObject,
	Properties: openapi.SchemaObject{
		{
			Name: "field_param",
			Schema: &openapi.Schema{
				Type: openapi.SchemaTypeString,
			},
		},
	},
}

var result = &openapi.Schema{
	Reference:   openapi.Reference{Name: "Output"},
	Title:       "Output",
	Description: "Output params of the method_name.",
	Type:        openapi.SchemaTypeObject,
	Properties: openapi.SchemaObject{
		{
			Name: "field_result",
			Schema: &openapi.Schema{
				Type: openapi.SchemaTypeString,
			},
		},
	},
}

var spec = &openapi.OpenAPI{
	Openapi: "3.0.3",
	Info: &openapi.Info{
		Title:          "Info.Title",
		Description:    "Info.Description",
		TermsOfService: "http://localhost:8080/info-TermsOfService",
		Contact: &openapi.Contact{
			Name:  "admin",
			URL:   "http://localhost:8080/info-contactAdmin",
			Email: "email@email.org",
		},
		Version: "v1.0.0",
	},
	Servers: []*openapi.Server{
		{URL: "/rpc", Description: "RPC Service"},
	},
	Paths: openapi.PathObject{
		{
			Path: "/rpc#method_mame",
			Object: &openapi.PathItem{
				Summary:     "Summary of the method_mame.",
				Description: "Description of the method_mame\nThe second line.\n",
				Post: &openapi.Operation{
					Tags:        []string{"SomeTag"},
					OperationID: "method_mame",
					RequestBody: &openapi.RequestBody{
						Content: map[string]*openapi.MediaType{
							"application/json": {
								Schema: &openapi.Schema{
									Title: "Request body of the method_mame",
									Type:  openapi.SchemaTypeObject,
									Properties: openapi.SchemaObject{
										{
											Name: "jsonrpc",
											Schema: &openapi.Schema{
												Description: `A String specifying the version of the JSON-RPC protocol. MUST be exactly "2.0".`,
												Type:        openapi.SchemaTypeString,
												Enum:        []interface{}{"2.0"},
											},
										},
										{
											Name: "id",
											Schema: &openapi.Schema{
												Description: `An identifier established by the Client.`,
												Type:        openapi.SchemaTypeString,
												Format:      openapi.SchemaFormat("uuid"),
											},
										},
										{
											Name: "method",
											Schema: &openapi.Schema{
												Description: `A String containing the name of the method to be invoked.`,
												Type:        openapi.SchemaTypeString,
												Enum:        []interface{}{"method_name"},
											},
										},
										{
											Name:   "params",
											Schema: params.GetReferenceObject(),
										},
									},
								},
							},
						},
						Required: true,
					},
					Responses: map[string]*openapi.Response{
						"200": {
							Description: "Response body of the method_name.",
							Content: map[string]*openapi.MediaType{
								"application/json": {
									Schema: &openapi.Schema{
										Title: "Request body of the method_mame",
										Type:  openapi.SchemaTypeObject,
										Properties: openapi.SchemaObject{
											{
												Name: "jsonrpc",
												Schema: &openapi.Schema{
													Description: `A String specifying the version of the JSON-RPC protocol. MUST be exactly "2.0".`,
													Type:        openapi.SchemaTypeString,
													Enum:        []interface{}{"2.0"},
												},
											},
											{
												Name: "id",
												Schema: &openapi.Schema{
													Description: `It MUST be the same as the value of the id member in the Request.`,
													Type:        openapi.SchemaTypeString,
													Format:      openapi.SchemaFormat("uuid"),
												},
											},
											{
												Name:   "error",
												Schema: rpcError.GetReferenceObject(),
											},
											{
												Name:   "result",
												Schema: result.GetReferenceObject(),
											},
										},
									},
								},
							},
						},
					},
				},
			},
		},
	},
	Components: &openapi.Components{
		Schemas: openapi.SchemaObject{
			{
				Name:   rpcError.Reference.Name,
				Schema: rpcError,
			},
			{
				Name:   params.Reference.Name,
				Schema: params,
			},
			{
				Name:   result.Reference.Name,
				Schema: result,
			},
		},
		SecuritySchemes: openapi.SecuritySchemeObject{
			{
				Name: "basicAuth",
				Scheme: &openapi.SecurityScheme{
					Type:        openapi.SecuritySchemeTypeHTTP,
					Description: "basic auth of the service",
					Scheme:      "basic",
				},
			},
		},
	},
	Security: []openapi.SecurityRequirementObject{
		{
			{
				Name: "basicAuth",
				List: []string{},
			},
		},
	},
	Tags: []*openapi.Tag{
		{
			Name:        "SomeTag",
			Description: "SomeTag Description",
		},
	},
}

func TestJSONTest(t *testing.T) {
	t.Parallel()

	testCase := func(data *openapi.OpenAPI, file string) {
		t.Helper()

		// Marshal test spec into JSON.
		jsonData, err := json.MarshalIndent(data, "", "  ")
		if err != nil {
			t.Fatal("Marshal data:", err)
		}

		fileData, err := os.ReadFile(file)
		if err != nil {
			t.Fatal("os.ReadFile:", err)
		}

		// And compare result with our test file.
		if !bytes.Equal(fileData, jsonData) {
			t.Log(string(jsonData))
			t.Fatalf("wrong json data:\n%q\n%q\n", string(fileData), string(jsonData))
		}

		// Now we have to check unmarshal methods.
		// Unmarshal file content into spec type.
		dst := new(openapi.OpenAPI)
		if err = json.Unmarshal(fileData, dst); err != nil {
			t.Fatal("Unmarshal fileData:", err)
		}

		// And marshal it back.
		dstData, err := json.MarshalIndent(dst, "", "  ")
		if err != nil {
			t.Fatal("Marshal dstData:", err)
		}

		// It have be the same as file.
		if !bytes.Equal(fileData, dstData) {
			t.Log(string(dstData))
			t.Fatalf("wrong json data:\n%q\n%q\n", string(fileData), string(dstData))
		}
	}

	testCase(spec, filepath.Join("testdata", "swagger.json"))
}

func TestGetReferenceObject(t *testing.T) {
	t.Parallel()

	type referrer interface {
		GetRef() string
	}

	testCase := func(refer referrer, want string) {
		if refer.GetRef() != want {
			t.Fatalf("wrong ref:\nwant %q\ngot  %q", want, refer.GetRef())
		}
	}

	testCase(
		(&openapi.Schema{Reference: openapi.Reference{Name: "Name"}}).GetReferenceObject(),
		"#/components/schemas/Name",
	)

	testCase(
		(&openapi.Response{Reference: openapi.Reference{Name: "Name"}}).GetReferenceObject(),
		"#/components/responses/Name",
	)

	testCase(
		(&openapi.Parameter{Reference: openapi.Reference{Name: "Name"}}).GetReferenceObject(),
		"#/components/parameters/Name",
	)

	testCase(
		(&openapi.Example{Reference: openapi.Reference{Name: "Name"}}).GetReferenceObject(),
		"#/components/examples/Name",
	)

	testCase(
		(&openapi.RequestBody{Reference: openapi.Reference{Name: "Name"}}).GetReferenceObject(),
		"#/components/requestBodies/Name",
	)

	testCase(
		(&openapi.Header{Reference: openapi.Reference{Name: "Name"}}).GetReferenceObject(),
		"#/components/headers/Name",
	)

	testCase(
		(&openapi.Link{Reference: openapi.Reference{Name: "Name"}}).GetReferenceObject(),
		"#/components/links/Name",
	)
}
