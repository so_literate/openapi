// Package openapi contains models of the Open API (swagger) specification.
// https://swagger.io/specification <- origin spec.
package openapi

import (
	"fmt"
)

// OpenAPI is the root document object of the OpenAPI document.
type OpenAPI struct {
	// REQUIRED. This string MUST be the semantic version number of the OpenAPI Specification version
	// that the OpenAPI document uses. The openapi field SHOULD be used by tooling specifications and
	// clients to interpret the OpenAPI document. This is not related to the API info.version string.
	Openapi string `json:"openapi"`

	// REQUIRED. Provides metadata about the API. The metadata MAY be used by tooling as required.
	Info *Info `json:"info"`

	// An array of Server Objects, which provide connectivity information to a target server.
	// If the servers property is not provided, or is an empty array,
	// the default value would be a Server Object with a url value of /.
	Servers []*Server `json:"servers"`

	// REQUIRED. The available paths and operations for the API.
	// Holds the relative paths to the individual endpoints and their operations.
	// The path is appended to the URL from the Server Object in order to construct the full URL.
	// The Paths MAY be empty, due to ACL constraints.
	// "/path" -> *PathItem.
	Paths PathObject `json:"paths"`

	// An element to hold various schemas for the specification.
	Components *Components `json:"components,omitempty"`

	// A declaration of which security mechanisms can be used across the API.
	// The list of values includes alternative security requirement objects that can be used.
	// Only one of the security requirement objects need to be satisfied to authorize a request.
	// Individual operations can override this definition.
	// To make security optional, an empty security requirement ({}) can be included in the array.
	Security []SecurityRequirementObject `json:"security,omitempty"`

	// A list of tags used by the specification with additional metadata.
	// The order of the tags can be used to reflect on their order by the parsing tools.
	// Not all tags that are used by the Operation Object must be declared.
	// The tags that are not declared may be organized randomly or based on the tools' logic.
	// Each tag name in the list MUST be unique.
	Tags []*Tag `json:"tags,omitempty"`

	// Additional external documentation.
	ExternalDocs *ExternalDocumentation `json:"externalDocs,omitempty"`
}

// Info provides metadata about the API. The metadata MAY be used by the clients if needed,
// and MAY be presented in editing or documentation generation tools for convenience.
type Info struct {
	// REQUIRED. The title of the API.
	Title string `json:"title"`

	// A short description of the API. CommonMark syntax MAY be used for rich text representation.
	Description string `json:"description,omitempty"`

	// A URL to the Terms of Service for the API. MUST be in the format of a URL.
	TermsOfService string `json:"termsOfService,omitempty"`

	// The contact information for the exposed API.
	Contact *Contact `json:"contact,omitempty"`

	// The license information for the exposed API.
	License *License `json:"license,omitempty"`

	// REQUIRED. The version of the OpenAPI document (which is distinct from
	// the OpenAPI Specification version or the API implementation version).
	Version string `json:"version"`
}

// Contact information for the exposed API.
type Contact struct {
	// The identifying name of the contact person/organization.
	Name string `json:"name,omitempty"`

	// The URL pointing to the contact information. MUST be in the format of a URL.
	URL string `json:"url,omitempty"`

	// The email address of the contact person/organization. MUST be in the format of an email address.
	Email string `json:"email,omitempty"`
}

// License information for the exposed API.
type License struct {
	// REQUIRED. The license name used for the API.
	Name string `json:"name"`

	// A URL to the license used for the API. MUST be in the format of a URL.
	URL string `json:"url,omitempty"`
}

// Server representing a Server.
type Server struct {
	// REQUIRED. A URL to the target host. This URL supports Server Variables and MAY be relative,
	// to indicate that the host location is relative to the location where
	// the OpenAPI document is being served. Variable substitutions will be
	// made when a variable is named in {brackets}.
	URL string `json:"url"`

	// An optional string describing the host designated by the URL.
	// CommonMark syntax MAY be used for rich text representation.
	Description string `json:"description,omitempty"`

	// A map between a variable name and its value.
	// The value is used for substitution in the server's URL template.
	Variables map[string]*ServerVariable `json:"variables,omitempty"`
}

// ServerVariable representing a Server Variable for server URL template substitution.
type ServerVariable struct {
	// An enumeration of string values to be used if the substitution options are from a limited set.
	// The array SHOULD NOT be empty.
	Enum []string `json:"enum,omitempty"`

	// REQUIRED. The default value to use for substitution, which SHALL be sent if
	// an alternate value is not supplied. Note this behavior is different than the
	// Schema Object's treatment of default values, because in those cases parameter
	// values are optional. If the enum is defined, the value SHOULD exist in the enum's values.
	Default string `json:"default"`

	// An optional description for the server variable.
	// CommonMark syntax MAY be used for rich text representation.
	Description string `json:"description,omitempty"`
}

// PathItem describes the operations available on a single path.
// A Path Item MAY be empty, due to ACL constraints.
// The path itself is still exposed to the documentation viewer but
// they will not know which operations and parameters are available.
type PathItem struct {
	// Allows for an external definition of this path item.
	// The referenced structure MUST be in the format of a Path Item Object.
	// In case a Path Item Object field appears both in the defined object
	// and the referenced object, the behavior is undefined.
	Ref string `json:"$ref,omitempty"`

	// An optional, string summary, intended to apply to all operations in this path.
	Summary string `json:"summary,omitempty"`

	// An optional, string description, intended to apply to all operations in this path.
	// CommonMark syntax MAY be used for rich text representation.
	Description string `json:"description,omitempty"`

	// A definition of a GET operation on this path.
	Get *Operation `json:"get,omitempty"`
	// A definition of a PUT operation on this path.
	Put *Operation `json:"put,omitempty"`
	// A definition of a POST operation on this path.
	Post *Operation `json:"post,omitempty"`
	// A definition of a DELETE operation on this path.
	Delete *Operation `json:"delete,omitempty"`
	// A definition of a OPTIONS operation on this path.
	Options *Operation `json:"options,omitempty"`
	// A definition of a HEAD operation on this path.
	Head *Operation `json:"head,omitempty"`
	// A definition of a PATCH operation on this path.
	Patch *Operation `json:"patch,omitempty"`
	// A definition of a TRACE operation on this path.
	Trace *Operation `json:"trace,omitempty"`

	// An alternative server array to service all operations in this path.
	Server []*Server `json:"servers,omitempty"`

	// A list of parameters that are applicable for all the operations described under this path.
	// These parameters can be overridden at the operation level, but cannot be removed there.
	// The list MUST NOT include duplicated parameters.
	// A unique parameter is defined by a combination of a name and location.
	// The list can use the Reference Object to link to parameters that
	// are defined at the OpenAPI Object's components/parameters.
	Parameters []*Parameter `json:"parameters,omitempty"`
}

// Operation describes a single API operation on a path.
type Operation struct {
	// A list of tags for API documentation control.
	// Tags can be used for logical grouping of operations by resources or any other qualifier.
	Tags []string `json:"tags,omitempty"`

	// A short summary of what the operation does.
	Summary string `json:"summary,omitempty"`

	// A verbose explanation of the operation behavior.
	// CommonMark syntax MAY be used for rich text representation.
	Description string `json:"description,omitempty"`

	// Additional external documentation for this operation.
	ExternalDocs *ExternalDocumentation `json:"externalDocs,omitempty"`

	// Unique string used to identify the operation.
	// The id MUST be unique among all operations described in the API.
	// The operationId value is case-sensitive.
	// Tools and libraries MAY use the operationId to uniquely identify an operation,
	// therefore, it is RECOMMENDED to follow common programming naming conventions.
	OperationID string `json:"operationId,omitempty"`

	// A list of parameters that are applicable for this operation.
	// If a parameter is already defined at the Path Item, the new definition will
	// override it but can never remove it.
	// The list MUST NOT include duplicated parameters.
	// A unique parameter is defined by a combination of a name and location.
	// The list can use the Reference Object to link to parameters that
	// are defined at the OpenAPI Object's components/parameters.
	Parameters []*Parameter `json:"parameters,omitempty"`

	// The request body applicable for this operation.
	// The requestBody is only supported in HTTP methods where the HTTP 1.1
	// specification RFC7231 has explicitly defined semantics for request bodies.
	// In other cases where the HTTP spec is vague, requestBody SHALL be ignored by consumers.
	RequestBody *RequestBody `json:"requestBody,omitempty"`

	// REQUIRED. The list of possible responses as they are returned from executing this operation.
	// {HTTP Status Code} -> *Response. Example: "200":{...}.
	Responses map[string]*Response `json:"responses"`

	// Declares this operation to be deprecated.
	// Consumers SHOULD refrain from usage of the declared operation.
	// Default value is false.
	Deprecated bool `json:"deprecated,omitempty"`

	// A declaration of which security mechanisms can be used for this operation.
	// The list of values includes alternative security requirement objects that can be used.
	// Only one of the security requirement objects need to be satisfied to authorize a request.
	// To make security optional, an empty security requirement ({}) can be included in the array.
	// This definition overrides any declared top-level security.
	// To remove a top-level security declaration, an empty array can be used.
	Security SecurityRequirementObject `json:"security,omitempty"`

	// An alternative server array to service this operation.
	// If an alternative server object is specified at the Path Item Object or Root level,
	// it will be overridden by this value.
	Servers []*Server `json:"servers,omitempty"`
}

// ExternalDocumentation allows referencing an external resource for extended documentation.
type ExternalDocumentation struct {
	// A short description of the target documentation.
	// CommonMark syntax MAY be used for rich text representation.
	Description string `json:"description,omitempty"`

	// REQUIRED. The URL for the target documentation. Value MUST be in the format of a URL.
	URL string `json:"url"`
}

// ParameterIn is a location of the parameter.
type ParameterIn string

// Possible values of location of the parameter.
const (
	ParameterInQuery  ParameterIn = "query"
	ParameterInHeader ParameterIn = "header"
	ParameterInPath   ParameterIn = "path"
	ParameterInCookie ParameterIn = "cookie"
)

// Parameter describes a single operation parameter.
type Parameter struct {
	Reference

	// REQUIRED. The name of the parameter. Parameter names are case sensitive.
	// - If in is "path", the name field MUST correspond to a
	// template expression occurring within the path field in the
	// Paths Object. See Path Templating for further information.
	// - If in is "header" and the name field is "Accept", "Content-Type" or "Authorization",
	// the parameter definition SHALL be ignored.
	// - For all other cases, the name corresponds to the parameter name used by the in property.
	Name string `json:"name,omitempty"`

	// REQUIRED. The location of the parameter.
	// Possible values are "query", "header", "path" or "cookie".
	In ParameterIn `json:"in,omitempty"`

	// A brief description of the parameter. This could contain examples of use.
	// CommonMark syntax MAY be used for rich text representation.
	Description string `json:"description,omitempty"`

	// Determines whether this parameter is mandatory.
	// If the parameter location is "path", this property is REQUIRED and its value MUST be true.
	// Otherwise, the property MAY be included and its default value is false.
	Required bool `json:"required,omitempty"`

	// Specifies that a parameter is deprecated and SHOULD be transitioned out of usage.
	// Default value is false.
	Deprecated bool `json:"deprecated,omitempty"`

	// Describes how the parameter value will be serialized depending on the type of the parameter value.
	// Default values (based on value of in):
	// for query - form; for path - simple; for header - simple; for cookie - form.
	Style ParameterStyle `json:"style,omitempty"`

	// When this is true, parameter values of type array or object generate separate parameters
	// for each value of the array or key-value pair of the map.
	// For other types of parameters this property has no effect.
	// When style is form, the default value is true.
	// For all other styles, the default value is false.
	Explode bool `json:"explode,omitempty"`

	// Determines whether the parameter value SHOULD allow reserved characters, as defined by RFC3986
	// :/?#[]@!$&'()*+,;= to be included without percent-encoding.
	// This property only applies to parameters with an in value of query.
	// The default value is false.
	AllowReserved bool `json:"allowReserved,omitempty"`

	// The schema defining the type used for the parameter.
	Schema *Schema `json:"schema,omitempty"`

	// Example of the parameter's potential value.
	// The example SHOULD match the specified schema and encoding properties if present.
	// The example field is mutually exclusive of the examples field.
	// Furthermore, if referencing a schema that contains an example,
	// the example value SHALL override the example provided by the schema.
	// To represent examples of media types that cannot naturally be represented in JSON or YAML,
	// a string value can contain the example with escaping where necessary.
	Example interface{} `json:"example,omitempty"`

	// Examples of the parameter's potential value.
	// Each example SHOULD contain a value in the correct format as specified in the parameter encoding.
	// The examples field is mutually exclusive of the example field.
	// Furthermore, if referencing a schema that contains an example,
	// the examples value SHALL override the example provided by the schema.
	Examples map[string]*Example `json:"examples,omitempty"`

	// A map containing the representations for the parameter.
	// The key is the media type and the value describes it.
	// The map MUST only contain one entry.
	// "application/json": {...}.
	Content map[string]*MediaType `json:"content,omitempty"`
}

// ParameterStyle is a list of the values.
type ParameterStyle string

// Style Values.
const (
	// ParameterStyleMatrix: type: primitive, array, object | in: path
	// Path-style parameters defined by RFC6570.
	ParameterStyleMatrix ParameterStyle = "matrix"

	// ParameterStyleLabel: type: primitive, array, object | in:  	path
	// Label style parameters defined by RFC6570.
	ParameterStyleLabel ParameterStyle = "label"

	// ParameterStyleForm: type: primitive, array, object | in: query, cookie
	// Form style parameters defined by RFC6570.
	// This option replaces collectionFormat with a csv (when explode is false) or
	// multi (when explode is true) value from OpenAPI 2.0.
	ParameterStyleForm ParameterStyle = "form"

	// ParameterStyleSimple: type: array | in: path, header
	// Simple style parameters defined by RFC6570.
	// This option replaces collectionFormat with a csv value from OpenAPI 2.0.
	ParameterStyleSimple ParameterStyle = "simple"

	// ParameterStyleSpaceDelimited: type: array | in: query
	// Space separated array values. This option replaces collectionFormat equal to ssv from OpenAPI 2.0.
	ParameterStyleSpaceDelimited ParameterStyle = "spaceDelimited"

	// ParameterStylePipeDelimited: type: array | in: query
	// Pipe separated array values. This option replaces collectionFormat equal to pipes from OpenAPI 2.0.
	ParameterStylePipeDelimited ParameterStyle = "pipeDelimited"

	// ParameterStyleDeepObject: type: object | in: query
	// Provides a simple way of rendering nested objects using form parameters.
	ParameterStyleDeepObject ParameterStyle = "deepObject"
)

// Example describes example of the parameter and etc.
type Example struct {
	Reference

	// Short description for the example.
	Summary string `json:"summary,omitempty"`

	// Long description for the example.
	// CommonMark syntax MAY be used for rich text representation.
	Description string `json:"description,omitempty"`

	// Embedded literal example. The value field and externalValue field are mutually exclusive.
	// To represent examples of media types that cannot naturally represented in JSON or YAML,
	// use a string value to contain the example, escaping where necessary.
	Value interface{} `json:"value,omitempty"`

	// A URL that points to the literal example.
	// This provides the capability to reference examples that cannot easily be
	// included in JSON or YAML documents.
	// The value field and externalValue field are mutually exclusive.
	ExternalValue string `json:"externalValue,omitempty"`
}

// RequestBody describes a single request body.
type RequestBody struct {
	Reference

	// A brief description of the request body. This could contain examples of use.
	// CommonMark syntax MAY be used for rich text representation.
	Description string `json:"description,omitempty"`

	// REQUIRED. The content of the request body.
	// The key is a media type or media type range and the value describes it.
	// For requests that match multiple keys, only the most specific key is
	// applicable. e.g. text/plain overrides text/*.
	Content map[string]*MediaType `json:"content"`

	// Determines if the request body is required in the request. Defaults to false.
	Required bool `json:"required,omitempty"`
}

// MediaType each Media Type Object provides schema and examples for the media type identified by its key.
type MediaType struct {
	// The schema defining the content of the request, response, or parameter.
	Schema *Schema `json:"schema,omitempty"`

	// Example of the media type.
	// The example object SHOULD be in the correct format as specified by the media type.
	// The example field is mutually exclusive of the examples field.
	// Furthermore, if referencing a schema which contains an example,
	// the example value SHALL override the example provided by the schema.
	Example string `json:"example,omitempty"`

	// Examples of the media type.
	// Each example object SHOULD match the media type and specified schema if present.
	// The examples field is mutually exclusive of the example field.
	// Furthermore, if referencing a schema which contains an example,
	// the examples value SHALL override the example provided by the schema.
	Examples map[string]*Example `json:"examples,omitempty"`

	// A map between a property name and its encoding information.
	// The key, being the property name, MUST exist in the schema as a property.
	// The encoding object SHALL only apply to requestBody objects when the media type
	// is multipart or application/x-www-form-urlencoded.
	Encoding map[string]*Encoding `json:"encoding,omitempty"`
}

// Encoding is a single encoding definition applied to a single schema property.
type Encoding struct {
	// The Content-Type for encoding a specific property.
	// Default value depends on the property type:
	// for string with format being binary – application/octet-stream;
	// for other primitive types – text/plain; for object - application/json;
	// for array – the default is defined based on the inner type.
	// The value can be a specific media type (e.g. application/json),
	// a wildcard media type (e.g. image/*), or a comma-separated list of the two types.
	ContentType string `json:"contentType,omitempty"`

	// A map allowing additional information to be provided as headers,
	// for example Content-Disposition. Content-Type is described separately and
	// SHALL be ignored in this section.
	// This property SHALL be ignored if the request body media type is not a multipart.
	Headers map[string]*Header `json:"headers,omitempty"`

	// Describes how a specific property value will be serialized depending on its type.
	// See Parameter Object for details on the style property.
	// The behavior follows the same values as query parameters, including default values.
	// This property SHALL be ignored if the request body media type is not application/x-www-form-urlencoded.
	Style ParameterStyle `json:"style,omitempty"`

	// When this is true, property values of type array or object generate separate parameters
	// for each value of the array, or key-value-pair of the map.
	// For other types of properties this property has no effect.
	// When style is form, the default value is true.
	// For all other styles, the default value is false.
	// This property SHALL be ignored if the request body media type is not application/x-www-form-urlencoded.
	Explode bool `json:"explode,omitempty"`

	// Determines whether the parameter value SHOULD allow reserved characters,
	// as defined by RFC3986 :/?#[]@!$&'()*+,;=
	// to be included without percent-encoding.
	// The default value is false.
	// This property SHALL be ignored if the request body media type is not application/x-www-form-urlencoded.
	AllowReserved bool `json:"allowReserved,omitempty"`
}

// The Header Object follows the structure of the Parameter Object with the following changes:
// 1. name MUST NOT be specified, it is given in the corresponding headers map.
// 2. in MUST NOT be specified, it is implicitly in header.
// 3. All traits that are affected by the location MUST be applicable to a location of header (for example, style).
type Header struct {
	Reference

	// A brief description of the header. This could contain examples of use.
	// CommonMark syntax MAY be used for rich text representation.
	Description string `json:"description,omitempty"`

	// Determines whether this header is mandatory.
	Required bool `json:"required,omitempty"`

	// Specifies that a header is deprecated and SHOULD be transitioned out of usage.
	// Default value is false.
	Deprecated bool `json:"deprecated,omitempty"`

	// Describes how the header value will be serialized depending on the type of the parameter value.
	Style ParameterStyle `json:"style,omitempty"`

	// When this is true, header values of type array or object generate separate parameters
	// for each value of the array or key-value pair of the map.
	// For other types of parameters this property has no effect.
	// When style is form, the default value is true.
	// For all other styles, the default value is false.
	Explode bool `json:"explode,omitempty"`

	// Determines whether the header value SHOULD allow reserved characters, as defined by RFC3986
	// :/?#[]@!$&'()*+,;= to be included without percent-encoding.
	// This property only applies to parameters with an in value of query.
	// The default value is false.
	AllowReserved bool `json:"allowReserved,omitempty"`

	// The schema defining the type used for the parameter.
	Schema *Schema `json:"schema,omitempty"`
}

// Response describes a single response from an API Operation.
type Response struct {
	Reference

	// REQUIRED. A short description of the response. CommonMark syntax MAY be used for rich text representation.
	Description string `json:"description,omitempty"`

	// Maps a header name to its definition.
	// RFC7230 states header names are case insensitive.
	// If a response header is defined with the name "Content-Type", it SHALL be ignored.
	Headers map[string]*Header `json:"headers,omitempty"`

	// A map containing descriptions of potential response payloads.
	// The key is a media type or media type range and the value describes it.
	// For responses that match multiple keys,
	// only the most specific key is applicable. e.g. text/plain overrides text/*.
	Content map[string]*MediaType `json:"content,omitempty"`

	// A map of operations links that can be followed from the response.
	// The key of the map is a short name for the link,
	// following the naming constraints of the names for Component Objects.
	Links map[string]*Link `json:"links,omitempty"`
}

// Link object represents a possible design-time link for a response.
// The presence of a link does not guarantee the caller's ability to successfully invoke it,
// rather it provides a known relationship and traversal mechanism between responses and other operations.
// Unlike dynamic links (i.e. links provided in the response payload),
// the OAS linking mechanism does not require link information in the runtime response.
// For computing links, and providing instructions to execute them, a runtime expression
// is used for accessing values in an operation and using them as
// parameters while invoking the linked operation.
type Link struct {
	Reference

	// A relative or absolute URI reference to an OAS operation.
	// This field is mutually exclusive of the operationId field, and MUST point to an Operation Object.
	// Relative operationRef values MAY be used to locate an
	// existing Operation Object in the OpenAPI definition.
	OperationRef string `json:"operationRef,omitempty"`

	// The name of an existing, resolvable OAS operation, as defined with a unique operationId.
	// This field is mutually exclusive of the operationRef field.
	OperationID string `json:"operationId,omitempty"`

	// A map representing parameters to pass to an operation as
	// specified with operationId or identified via operationRef.
	// The key is the parameter name to be used, whereas the value can be a constant or
	// an expression to be evaluated and passed to the linked operation.
	// The parameter name can be qualified using the parameter location [{in}.]{name}
	// for operations that use the same parameter name in different locations (e.g. path.id).
	Parameters map[string]interface{} `json:"parameters,omitempty"`

	// A literal value or {expression} to use as a request body when calling the target operation.
	RequestBody interface{} `json:"requestBody,omitempty"`

	// A description of the link. CommonMark syntax MAY be used for rich text representation.
	Description string `json:"description,omitempty"`

	// A server object to be used by the target operation.
	Server *Server `json:"server,omitempty"`
}

// Tag adds metadata to a single tag that is used by the Operation Object.
// It is not mandatory to have a Tag Object per tag defined in the Operation Object instances.
type Tag struct {
	// REQUIRED. The name of the tag.
	Name string `json:"name"`

	// A short description for the tag.
	// CommonMark syntax MAY be used for rich text representation.
	Description string `json:"description,omitempty"`

	// Additional external documentation for this tag.
	ExternalDocs *ExternalDocumentation `json:"externalDocs,omitempty"`
}

// SchemaType is a primitive data types in the Swagger Specification are based on the
// types supported by the JSON-Schema Draft 4.
// Models are described using the Schema Object which is a subset of JSON Schema Draft 4.
type SchemaType string

// List of the types.
const (
	SchemaTypeInteger SchemaType = "integer"
	SchemaTypeNumber  SchemaType = "number"
	SchemaTypeString  SchemaType = "string"
	SchemaTypeBoolean SchemaType = "boolean"
	SchemaTypeArray   SchemaType = "array"
	SchemaTypeObject  SchemaType = "object"
)

// SchemaFormat primitives have an optional modifier property format.
// Swagger uses several known formats to more finely define the data type being used.
// However, the format property is an open string-valued property,
// and can have any value to support documentation needs.
// Formats such as "email", "uuid", etc., can be used even though
// they are not defined by this specification.
type SchemaFormat string

// List of the schema formats.
const (
	// SchemaFormatInt32 + SchemaTypeInteger -> signed 32 bits.
	SchemaFormatInt32 SchemaFormat = "int32"
	// SchemaFormatInt64 + SchemaTypeInteger -> signed 64 bits.
	SchemaFormatInt64 SchemaFormat = "int64"
	// SchemaFormatFloat + SchemaTypeNumber.
	SchemaFormatFloat SchemaFormat = "float"
	// SchemaFormatDouble + SchemaTypeNumber.
	SchemaFormatDouble SchemaFormat = "double"
	// SchemaFormatByte + SchemaTypeString -> base64 encoded characters.
	SchemaFormatByte SchemaFormat = "byte"
	// SchemaFormatBinary + SchemaTypeString -> any sequence of octets.
	SchemaFormatBinary SchemaFormat = "binary"
	// SchemaFormatDate + SchemaTypeString -> as defined by full-date - RFC3339.
	SchemaFormatDate SchemaFormat = "date"
	// SchemaFormatDateTime + SchemaTypeString -> as defined by date-time - RFC3339.
	SchemaFormatDateTime SchemaFormat = "date-time"
	// SchemaFormatPassword + SchemaTypeString -> used to hint UIs the input needs to be obscured.
	SchemaFormatPassword SchemaFormat = "password"
)

// The Schema Object allows the definition of input and output data types.
// These types can be objects, but also primitives and arrays.
// This object is an extended subset of the JSON Schema Specification Wright Draft 00.
// For more information about the properties, see JSON Schema Core and JSON Schema Validation.
// Unless stated otherwise, the property definitions follow the JSON Schema.
type Schema struct {
	Reference

	// Short title of the schema.
	Title string `json:"title,omitempty"`
	// A verbose explanation of the schema.
	Description string `json:"description,omitempty"`

	// Type of the schema.
	Type SchemaType `json:"type,omitempty"`
	// Format of the type schema.
	Format SchemaFormat `json:"format,omitempty"`

	// Schema of the "array" items.
	Items *Schema `json:"items,omitempty"`
	// Enum list of any type.
	Enum []interface{} `json:"enum,omitempty"`
	// List of the required properties.
	Required []string `json:"required,omitempty"`
	// Properties list of the properties if Type is object.
	// "propName" -> *Schema.
	Properties SchemaObject `json:"properties,omitempty"`
	// Field for Map/Dictionary Properties.
	AdditionalProperties *Schema `json:"additionalProperties,omitempty"`

	// AllOf inline or referenced schema MUST be of a Schema Object and not a standard JSON Schema.
	AllOf []*Schema `json:"allOf,omitempty"`
	// OneOf inline or referenced schema MUST be of a Schema Object and not a standard JSON Schema.
	OneOf []*Schema `json:"oneOf,omitempty"`
	// AnyOf inline or referenced schema MUST be of a Schema Object and not a standard JSON Schema.
	AnyOf []*Schema `json:"anyOf,omitempty"`

	// A true value adds "null" to the allowed type specified by the type keyword,
	// only if type is explicitly defined within the same Schema Object.
	// Other Schema Object constraints retain their defined behavior, and therefore may
	// disallow the use of null as a value. A false value leaves the specified or default
	// type unmodified. The default value is false.
	Nullable bool `json:"nullable,omitempty"`
	// Adds support for polymorphism. The discriminator is an object name that
	// is used to differentiate between other schemas which may satisfy the payload description.
	// See Composition and Inheritance for more details.
	Discriminator *Discriminator `json:"discriminator,omitempty"`
	// Relevant only for Schema "properties" definitions.
	// Declares the property as "read only".
	// This means that it MAY be sent as part of a response but SHOULD NOT be sent as part of the request.
	// If the property is marked as readOnly being true and is in the required list,
	// the required will take effect on the response only.
	// A property MUST NOT be marked as both readOnly and writeOnly being true.
	// Default value is false.
	ReadOnly bool `json:"readOnly,omitempty"`
	// Relevant only for Schema "properties" definitions.
	// Declares the property as "write only".
	// Therefore, it MAY be sent as part of a request but SHOULD NOT be sent as part of the response.
	// If the property is marked as writeOnly being true and is in the required list,
	// the required will take effect on the request only.
	// A property MUST NOT be marked as both readOnly and writeOnly being true.
	// Default value is false.
	WriteOnly bool `json:"writeOnly,omitempty"`
	// Additional external documentation for this schema.
	ExternalDocs *ExternalDocumentation `json:"externalDocs,omitempty"`
	// A free-form property to include an example of an instance for this schema.
	// To represent examples that cannot be naturally represented in JSON or YAML,
	// a string value can be used to contain the example with escaping where necessary.
	Example interface{} `json:"example,omitempty"`
	// Specifies that a schema is deprecated and SHOULD be transitioned out of usage.
	// Default value is false.
	Deprecated bool `json:"deprecated,omitempty"`
}

// Discriminator object:
// When request bodies or response payloads may be one of a number of different schemas,
// a discriminator object can be used to aid in serialization, deserialization, and validation.
// The discriminator is a specific object in a schema which is used to inform the consumer of the
// specification of an alternative schema based on the value associated with it.
// When using the discriminator, inline schemas will not be considered.
type Discriminator struct {
	// REQUIRED. The name of the property in the payload that will hold the discriminator value.
	PropertyName string `json:"propertyName,omitempty"`

	// An object to hold mappings between payload values and schema names or references.
	Mapping map[string]string `json:"mapping,omitempty"`
}

// SecuritySchemeType type of the security scheme.
type SecuritySchemeType string

// Valid values of SecuritySchemeType.
const (
	SecuritySchemeTypeAPIKey        SecuritySchemeType = "apiKey"
	SecuritySchemeTypeHTTP          SecuritySchemeType = "http"
	SecuritySchemeTypeOAuth2        SecuritySchemeType = "oauth2"
	SecuritySchemeTypeOpenIDConnect SecuritySchemeType = "openIdConnect"
)

// SecurityAPIKeyIn is a location of the API key.
type SecurityAPIKeyIn string

// Valid values of SecurityAPIKeyIn.
const (
	SecurityAPIKeyInQuery  SecurityAPIKeyIn = "query"
	SecurityAPIKeyInHeader SecurityAPIKeyIn = "header"
	SecurityAPIKeyInCookie SecurityAPIKeyIn = "cookie"
)

// OAuthFlow is a configuration details for a supported OAuth Flow.
type OAuthFlow struct {
	// REQUIRED. The authorization URL to be used for this flow. This MUST be in the form of a URL.
	//
	// oauth2 ("implicit", "authorizationCode").
	AuthorizationURL string `json:"authorizationUrl,omitempty"`

	// REQUIRED. The token URL to be used for this flow. This MUST be in the form of a URL.
	//
	// oauth2 ("password", "clientCredentials", "authorizationCode").
	TokenURL string `json:"tokenUrl,omitempty"`

	// The URL to be used for obtaining refresh tokens. This MUST be in the form of a URL.
	//
	// oauth2.
	RefreshURL string `json:"refreshUrl,omitempty"`

	// REQUIRED. The available scopes for the OAuth2 security scheme.
	// A map between the scope name and a short description for it. The map MAY be empty.
	//
	// oauth2.
	Scopes map[string]string `json:"scopes,omitempty"`
}

// OAuthFlows allows configuration of the supported OAuth Flows.
type OAuthFlows struct {
	// Configuration for the OAuth Implicit flow.
	Implicit *OAuthFlow `json:"implicit,omitempty"`

	// Configuration for the OAuth Resource Owner Password flow.
	Password *OAuthFlow `json:"password,omitempty"`

	// Configuration for the OAuth Client Credentials flow. Previously called application in OpenAPI 2.0.
	ClientCredentials *OAuthFlow `json:"clientCredentials,omitempty"`

	// Configuration for the OAuth Authorization Code flow. Previously called accessCode in OpenAPI 2.0.
	AuthorizationCode *OAuthFlow `json:"authorizationCode,omitempty"`
}

// SecurityScheme defines a security scheme that can be used by the operations.
// Supported schemes are HTTP authentication, an API key
// (either as a header, a cookie parameter or as a query parameter),
// OAuth2's common flows (implicit, password, client credentials and authorization code) as
// defined in RFC6749, and OpenID Connect Discovery.
type SecurityScheme struct {
	// REQUIRED. The type of the security scheme.
	Type SecuritySchemeType `json:"type,omitempty"`

	// A short description for security scheme.
	// CommonMark syntax MAY be used for rich text representation.
	Description string `json:"description,omitempty"`

	// REQUIRED. The name of the header, query or cookie parameter to be used.
	//
	// APIKey.
	Name string `json:"name,omitempty"`
	// REQUIRED. The location of the API key. Valid values are "query", "header" or "cookie".
	//
	// APIKey.
	In SecurityAPIKeyIn `json:"in,omitempty"`

	// REQUIRED. The name of the HTTP Authorization scheme to be
	// used in the Authorization header as defined in RFC7235.
	// The values used SHOULD be registered in the IANA Authentication Scheme registry.
	//
	// http.
	Scheme string `json:"scheme,omitempty"`
	// A hint to the client to identify how the bearer token is formatted.
	// Bearer tokens are usually generated by an authorization server,
	// so this information is primarily for documentation purposes.
	//
	// http ("bearer").
	BearerFormat string `json:"bearerFormat,omitempty"`

	// REQUIRED. An object containing configuration information for the flow types supported.
	//
	// oauth2.
	Flows *OAuthFlows `json:"flows,omitempty"`

	// REQUIRED. OpenId Connect URL to discover OAuth2 configuration values.
	// This MUST be in the form of a URL.
	//
	// openIdConnect.
	OpenIDConnectURL string `json:"openIdConnectUrl,omitempty"`
}

// Components holds a set of reusable objects for different aspects of the OAS.
// All objects defined within the components object will have no effect on the API
// unless they are explicitly referenced from properties outside the components object.
type Components struct {
	// An object to hold reusable Schema Objects.
	Schemas SchemaObject `json:"schemas,omitempty"`

	// An object to hold reusable Response Objects.
	Responses map[string]*Response `json:"responses,omitempty"`

	// An object to hold reusable Parameter Objects.
	Parameters map[string]*Parameter `json:"parameters,omitempty"`

	// An object to hold reusable Example Objects.
	Examples map[string]*Example `json:"examples,omitempty"`

	// An object to hold reusable Request Body Objects.
	RequestBodies map[string]*RequestBody `json:"requestBodies,omitempty"`

	// An object to hold reusable Header Objects.
	Headers map[string]*Header `json:"headers,omitempty"`

	// An object to hold reusable Security Scheme Objects.
	SecuritySchemes SecuritySchemeObject `json:"securitySchemes,omitempty"`

	// An object to hold reusable Link Objects.
	Links map[string]*Link `json:"links,omitempty"`
}

// Reference is a simple object to allow referencing other components in the specification, internally and externally.
// The Reference Object is defined by JSON Reference and follows the same structure, behavior and rules.
// For this specification, reference resolution is accomplished as defined by the
// JSON Reference specification and not by the JSON Schema specification.
type Reference struct {
	// REQUIRED. The reference string.
	Ref string `json:"$ref,omitempty"`
	// Name of the object in reference.
	Name string `json:"-"`
}

// GetRef method just for tests.
func (r Reference) GetRef() string {
	return r.Ref
}

// GetReferenceObject returns this object as reference object to components.
func (s *Schema) GetReferenceObject() *Schema {
	return &Schema{
		Reference: Reference{
			Ref: fmt.Sprintf("#/components/schemas/%s", s.Reference.Name),
		},
	}
}

// GetReferenceObject returns this object as reference object to components.
func (r *Response) GetReferenceObject() *Response {
	return &Response{
		Reference: Reference{
			Ref: fmt.Sprintf("#/components/responses/%s", r.Reference.Name),
		},
	}
}

// GetReferenceObject returns this object as reference object to components.
func (p *Parameter) GetReferenceObject() *Parameter {
	return &Parameter{
		Reference: Reference{
			Ref: fmt.Sprintf("#/components/parameters/%s", p.Reference.Name),
		},
	}
}

// GetReferenceObject returns this object as reference object to components.
func (e *Example) GetReferenceObject() *Example {
	return &Example{
		Reference: Reference{
			Ref: fmt.Sprintf("#/components/examples/%s", e.Reference.Name),
		},
	}
}

// GetReferenceObject returns this object as reference object to components.
func (r *RequestBody) GetReferenceObject() *RequestBody {
	return &RequestBody{
		Reference: Reference{
			Ref: fmt.Sprintf("#/components/requestBodies/%s", r.Reference.Name),
		},
	}
}

// GetReferenceObject returns this object as reference object to components.
func (h *Header) GetReferenceObject() *Header {
	return &Header{
		Reference: Reference{
			Ref: fmt.Sprintf("#/components/headers/%s", h.Reference.Name),
		},
	}
}

// GetReferenceObject returns this object as reference object to components.
func (l *Link) GetReferenceObject() *Link {
	return &Link{
		Reference: Reference{
			Ref: fmt.Sprintf("#/components/links/%s", l.Reference.Name),
		},
	}
}
