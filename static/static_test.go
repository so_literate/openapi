package static_test

import (
	"bytes"
	"embed"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"testing"

	"gitlab.com/so_literate/openapi/static"
)

var (
	//go:embed testdata/openapi
	openapi embed.FS
	//go:embed testdata/openapi/first
	oneSpec embed.FS
)

func TestHostStatic(t *testing.T) {
	t.Parallel()

	testCase := func(prefix string, files embed.FS, testfile string, wantCode int, wantContentType string) {
		t.Helper()

		handler := static.HostStatic(prefix, files)

		req, err := http.NewRequest(http.MethodGet, testfile, http.NoBody)
		if err != nil {
			t.Fatal("http.NewRequest", err)
		}

		rec := httptest.NewRecorder()

		handler.ServeHTTP(rec, req)
		resp := rec.Result()
		defer resp.Body.Close()

		if resp.StatusCode != wantCode {
			t.Fatalf("wrong code: %d", resp.StatusCode)
		}

		if header := resp.Header.Get("Content-Type"); header != wantContentType {
			t.Fatalf("wrong Content-Type: %q", header)
		}
	}

	testCase(
		"/swagger", static.Static,
		"/swagger/static/favicon-16x16.png", http.StatusOK, "image/png",
	)

	testCase(
		"", static.Static,
		"/static/favicon-16x16.png", http.StatusOK, "image/png",
	)

	testCase(
		"/", static.Static,
		"/static/favicon-16x16.png", http.StatusOK, "image/png",
	)

	testCase(
		"/", static.Static,
		"/swagger/favicon-16x16.png", http.StatusNotFound, "text/plain; charset=utf-8",
	)

	testCase(
		"/swagger", openapi,
		"/swagger/testdata/openapi/first/swagger.json", http.StatusOK, "application/json",
	)

	testCase(
		"/swagger", openapi,
		"/swagger/testdata/openapi/second/swagger.json", http.StatusOK, "application/json",
	)

	testCase(
		"/swagger/", oneSpec,
		"/swagger/testdata/openapi/first/swagger.json", http.StatusOK, "application/json",
	)

	testCase(
		"/swagger", oneSpec,
		"/swagger/testdata/openapi/second/swagger.json", http.StatusNotFound, "text/plain; charset=utf-8",
	)
}

func TestHandlerFunc(t *testing.T) {
	t.Parallel()

	data := &static.SwaggerTemplateData{
		StaticPrefix: "../static",
		Specs: []static.Spec{
			{
				Name: "petstore",
				URL:  "https://petstore.swagger.io/v2/swagger.json",
			},
			{
				Name: "one",
				URL:  "/openapi/first/swagger.json",
			},
			{
				Name: "two",
				URL:  "/openapi/second/swagger.json",
			},
		},
	}

	handler := static.HandlerFunc(data)

	req, err := http.NewRequest(http.MethodGet, "/", http.NoBody)
	if err != nil {
		t.Fatal(err)
	}

	rec := httptest.NewRecorder()

	handler.ServeHTTP(rec, req)
	resp := rec.Result()
	defer resp.Body.Close()

	gotBody, err := io.ReadAll(resp.Body)
	if err != nil {
		t.Fatal(err)
	}

	wantBody, err := os.ReadFile(filepath.Join(".", "testdata", "want_swagger_page.html"))
	if err != nil {
		t.Fatal(err)
	}

	if !bytes.Equal(wantBody, gotBody) {
		t.Fatalf("wring body:\n%q\n%q", wantBody, gotBody)
	}
}
