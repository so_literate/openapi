package static_test

import (
	"embed"
	"fmt"
	"net/http"
	"net/http/httptest"

	"gitlab.com/so_literate/openapi/static"
)

//go:embed testdata/openapi
var specs embed.FS

func Example() {
	data := &static.SwaggerTemplateData{
		StaticPrefix: "/swagger/static",
		Specs: []static.Spec{
			{
				Name: "first",
				URL:  "/swagger/testdata/openapi/first/swagger.json",
			},
			{
				Name: "second",
				URL:  "/swagger/testdata/openapi/second/swagger.json",
			},
		},
	}

	mux := http.NewServeMux()

	mux.Handle("/swagger", static.HandlerFunc(data))
	mux.Handle("/swagger/static/", static.HostStatic("/swagger", static.Static))
	mux.Handle("/swagger/testdata/openapi/", static.HostStatic("/swagger", specs))

	// Main page.
	rec := httptest.NewRecorder()
	req, err := http.NewRequest(http.MethodGet, "/swagger", http.NoBody)
	if err != nil {
		fmt.Println(err)
		return
	}
	mux.ServeHTTP(rec, req)
	fmt.Println("/swagger:", rec.Result().StatusCode)

	// Static from Swagger UI.
	rec = httptest.NewRecorder()
	req, err = http.NewRequest(http.MethodGet, "/swagger/static/favicon-16x16.png", http.NoBody)
	if err != nil {
		fmt.Println(err)
		return
	}
	mux.ServeHTTP(rec, req)
	fmt.Println("/swagger/static/favicon-16x16.png:", rec.Result().StatusCode)

	// OpenAPI spec.
	rec = httptest.NewRecorder()
	req, err = http.NewRequest(http.MethodGet, "/swagger/testdata/openapi/first/swagger.json", http.NoBody)
	if err != nil {
		fmt.Println(err)
		return
	}
	mux.ServeHTTP(rec, req)
	fmt.Println("/swagger/testdata/openapi/first/swagger.json:", rec.Result().StatusCode)

	// OpenAPI spec wrong.
	rec = httptest.NewRecorder()
	req, err = http.NewRequest(http.MethodGet, "/swagger/testdata/openapi/first/swagger.yaml", http.NoBody)
	if err != nil {
		fmt.Println(err)
		return
	}
	mux.ServeHTTP(rec, req)
	fmt.Println("/swagger/testdata/openapi/first/swagger.yaml:", rec.Result().StatusCode)

	// Output:
	// /swagger: 200
	// /swagger/static/favicon-16x16.png: 200
	// /swagger/testdata/openapi/first/swagger.json: 200
	// /swagger/testdata/openapi/first/swagger.yaml: 404
}
