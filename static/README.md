Static
========

The package contains static files of the Swagger UI web page.
Also contains methods to host specs and static files by own http server.

Current version of SwaggerUI is [v4.11.1](https://github.com/swagger-api/swagger-ui/releases/tag/v4.11.1).
