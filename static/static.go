// Package static contains static files of the Swagger UI web page.
// Also contains methods to host specs and static files by own http server.
package static

import (
	"bytes"
	"embed"
	"html/template"
	"io"
	"net/http"
)

var (
	// Static contains dist dir from SwaggerUI project.
	// https://github.com/swagger-api/swagger-ui/tree/master/dist
	// It's compiled vesrion of the web app.
	//go:embed static
	Static embed.FS

	//go:embed swagger.tmpl
	swaggerTmpl string

	// SwaggerTemplate is template of the main page with SwaggerUI.
	SwaggerTemplate = template.Must(template.New("swagger").Parse(swaggerTmpl))
)

// Spec is one of the specifactaion with file name and URL to the file.
type Spec struct {
	Name string // Name of the spec.
	URL  string // URL to spec (availbe to use without host just URL `/some/url`).
}

// SwaggerTemplateData data set to execute SwaggerUI template.
// SingleURL or Specs must be not empty.
type SwaggerTemplateData struct {
	StaticPrefix string // URL prefix of the static files.
	Specs        []Spec // List of the specs in dropdown menu with specs on the page.
}

// ExecuteSwaggerTemplate executes SwaggerUI template to the Writer.
func ExecuteSwaggerTemplate(w io.Writer, data *SwaggerTemplateData) error {
	return SwaggerTemplate.Execute(w, data) // nolint:wrapcheck // No additional context here.
}

// HandlerFunc returns handler func for http router. It call panic when impossible to execute template.
func HandlerFunc(data *SwaggerTemplateData) http.HandlerFunc {
	buf := bytes.Buffer{}
	err := ExecuteSwaggerTemplate(&buf, data)
	if err != nil {
		panic("unexpected error in ExecuteSwaggerTemplate: " + err.Error())
	}

	return func(w http.ResponseWriter, _ *http.Request) {
		w.WriteHeader(http.StatusOK)
		w.Write(buf.Bytes()) // nolint:errcheck,gosec // No handler here.
	}
}

// HostStatic serves static files embedded into Go project.
// You must pass prefix if you have url prefix before static, for example:
//   /some/prefix/static => HostStatic("/some/prefix/", static.Static)
// This method will be search in /some/prefix/static/filename.js
// but Static contains only /static/filename.js.
func HostStatic(prefix string, efs embed.FS) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		fs := http.StripPrefix(prefix, http.FileServer(http.FS(efs)))
		fs.ServeHTTP(w, r)
	}
}
